package com.example.diploma

import android.content.Context

class SPManager(context: Context) {
    private  val USER_UID = "UID"
    private  val SHARED_PREF_NAME = "itpec diploma"
    private val USER_NAME = "NAME"
    private val USER_AVATAR ="AVATAR"
    private val YEAR ="year"
    private val TYPE ="TYPE"
    private val FIELD ="FIELD"
    private val ISAM = "clock"



    private val spManager = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

    fun setUserUid(uid:String){ spManager.edit().putString(USER_UID,uid).apply()}
    fun getUserUid():String{ return spManager.getString(USER_UID,"")!!}

    fun setUserName(name:String){spManager.edit().putString(USER_NAME,name).apply()}
    fun getUserName():String{ return spManager.getString(USER_NAME,"")!!}

    fun setUserAvatar(avatar:String){ spManager.edit().putString(USER_AVATAR,avatar).apply()}
    fun getUserAvatar():String{ return  spManager.getString(USER_AVATAR,"")!!}

    fun setExerciseYear(year:Int){ spManager.edit().putInt(YEAR,year).apply()}
    fun getExerciseYear():Int{ return  spManager.getInt(YEAR,0)}

    fun setExerciseType(type:String){ spManager.edit().putString(TYPE,type).apply()}
    fun getExerciseType():String{ return  spManager.getString(TYPE,"")!!}

    fun setExerciseField(field:String){ spManager.edit().putString(FIELD,field).apply()}
    fun getExerciseField():String{ return  spManager.getString(FIELD,"")!!}

    fun setExerciseIsam(isam:Boolean){ spManager.edit().putBoolean(ISAM,isam).apply()}
    fun getExerciseIsam():Boolean{ return  spManager.getBoolean(ISAM,true)}

    fun resetAll(){
        setUserUid("")
        setUserName("")
        setUserAvatar("")


    }

    fun resetExercise(){
        setExerciseYear(0)
        setExerciseType("")
        setExerciseField("")
        setExerciseIsam(false)
    }

}