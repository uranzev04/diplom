package com.example.diploma

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.example.diploma.adapters.MainExamTypeAdapter
import com.example.diploma.adapters.MainViewPagerAdapter
import com.example.diploma.fragments.*
import com.example.diploma.interfaces.RecyclerItemClickListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
/**
 *Created By Uranzev
 * 2019
 */
class MainActivity : AppCompatActivity() {
    private lateinit var db:FirebaseFirestore
    private lateinit var examTypes:ArrayList<ExamType>
    private lateinit var adapter :MainExamTypeAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var mainAdapter:MainViewPagerAdapter
    private val historyFragment = HistoryFragment()
    private val leaderBoardFragment = LeaderBoardFragment()
    private val statisticFragment = StatisticFragment()
    private val exerciseFragment = ExerciseFragment()
    private var isEnglish = false

    private var menuFragment = NavigationFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = FirebaseFirestore.getInstance()

        readExamTypes()
//        readUsersFromFireBase()
//        readQuestionsFromFireBase()
        setNavigation()
        initTabs()
        mainHeaderImage.setOnClickListener {
            drawView.openDrawer(navigation)
        }
        mainTabLeft.setOnClickListener {
           leftSelected()
        }
        mainTabRight.setOnClickListener {
           rightSelected()
        }
        upload.setOnClickListener {
            uploadQuetsion()
        }
    }
    private fun uploadQuetsion(){
        val question = Questions()
        question.question = "Энтерпрайс архитектурын “дөрвөн домайн”-д ордог архитектур дотроос дараахаас аль нь бизнесийн архитектур, технологийн архитектур, болон хэрэглээний архитектураас өөр архитектур вэ?"
        question.answer1 = "Шугамын архитектур"
        question.answer2 = "Өгөгдлийн архитектур"
        question.answer3 = "Програм хангамжийн архитектур"
        question.answer4 = "Системийн архитектур"
        question.am = true
        question.correctAnswer = 2
        question.isAnswersImage = false
        question.examType ="FE"
        question.point = 2
        question.category = "system strategy"
        question.year = 2019
        uploadQuetsionClass(question)

    }

    private fun uploadQuetsionClass(data:Questions){
        db.collection("questions")
            .add(data)
            .addOnSuccessListener {
                Log.e("upload","success")
            }
            .addOnFailureListener {
                Log.e("upload","error =>$it")
            }
    }

    private fun leftSelected(){
        isEnglish = false
        mainViewPager.currentItem = 0
        mainTabLeft.background = ContextCompat.getDrawable(this,R.drawable.fill_rounded_left)
        mainMongolText.setTextColor(ContextCompat.getColor(this,R.color.colorWhite))
        mainTabRight.background = null
        mainEnglishText.setTextColor(ContextCompat.getColor(this,R.color.colorPaleBlue))
    }

    private fun rightSelected(){
        isEnglish = true
        mainViewPager.currentItem = 1
        mainTabRight.background = ContextCompat.getDrawable(this,R.drawable.fill_rounded_right)
        mainEnglishText.setTextColor(ContextCompat.getColor(this,R.color.colorWhite))
        mainTabLeft.background = null
        mainMongolText.setTextColor(ContextCompat.getColor(this,R.color.colorPaleBlue))
    }

    private fun setNavigation(){
        supportFragmentManager.beginTransaction().replace(R.id.navigationContainer, menuFragment).addToBackStack("navi").commit()

    }

    fun showHistoryFragment(){
        closeDrawer()
        supportFragmentManager.beginTransaction().replace(R.id.mainContainer,historyFragment).addToBackStack(historyFragment::class.java.name).commit()
    }
    private fun closeDrawer(){
        if(drawView.isDrawerOpen(Gravity.LEFT))
            drawView.closeDrawer(Gravity.LEFT)
    }

    fun showLeaderBoardFragment(){
        closeDrawer()
        supportFragmentManager.beginTransaction().replace(R.id.mainContainer,leaderBoardFragment).addToBackStack(leaderBoardFragment::class.java.name).commit()
    }

    fun showStatisticFragment(){
        closeDrawer()
        supportFragmentManager.beginTransaction().replace(R.id.mainContainer,statisticFragment).addToBackStack(statisticFragment::class.java.name).commit()
    }

    fun showExerciseFragment(){
        closeDrawer()
        supportFragmentManager.beginTransaction().replace(R.id.mainContainer,exerciseFragment).addToBackStack(exerciseFragment::class.java.name).commit()
    }

    private fun initTabs(){
        mainTabLeft.background = ContextCompat.getDrawable(this,R.drawable.fill_rounded_left)
        mainMongolText.setTextColor(ContextCompat.getColor(this,R.color.colorWhite))

    }



    private fun readExamTypes(){
        db.collection("examType")
            .get()
            .addOnSuccessListener {
                examTypes = ArrayList()
                for(document in it){
                    val type = Gson().fromJson(Gson().toJson(document.data),ExamType::class.java)
                    Log.e("document","type=>${type.name}")

                    examTypes.add(type)
                }
                setViewDatas()
            }
            .addOnFailureListener {
                Log.e("document","error")
            }
    }
     fun startExam(data:ExamType){
         val intent = Intent(this,ExamActivity::class.java)
         intent.putExtra("type",data.type)
         intent.putExtra("isAm",data.isAm)
         intent.putExtra("time",data.durationTime)
         Log.e("main","type=>${data.type}")
         intent.putExtra("totalQuestion",data.totalQuestion)
         SPManager(this).resetExercise()
         startActivity(intent)
    }
    private fun setViewDatas(){
        adapter = MainExamTypeAdapter(object: RecyclerItemClickListener {
            override fun onItemClickListener(position: Int, data: Any) {
                (data as ExamType)
                startExam(data)
            }
        },false)
        adapter.setDatas(examTypes)
        layoutManager = LinearLayoutManager(this)
        mainRecyclerView.layoutManager = layoutManager
        mainRecyclerView.adapter = adapter
        mainAdapter = MainViewPagerAdapter(supportFragmentManager, examTypes)
        mainViewPager.adapter = mainAdapter
        mainViewPager.addOnPageChangeListener(object:ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
               when(position){
                   0 -> leftSelected()
                   1 -> rightSelected()
                   else ->{}
               }
            }

        })


    }

    override fun onBackPressed() {
        when{
            drawView.isDrawerOpen(Gravity.LEFT) -> drawView.closeDrawer(Gravity.LEFT)
            supportFragmentManager.backStackEntryCount == 1 -> finish()
            else -> super.onBackPressed()
        }



    }


}
