package com.example.diploma.commonUtils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.view.LayoutInflater
import android.view.View
import com.example.diploma.ConfirmDialogProperty
import com.example.diploma.R
import com.example.diploma.interfaces.DialogButtonClickListener
import kotlinx.android.synthetic.main.simple_confirm_dialog.view.*

object CommonDialog {
    fun showConfirmDialog(context: Context, property: ConfirmDialogProperty, listener:DialogButtonClickListener, showEditText:Boolean = false){
        val view = LayoutInflater.from(context).inflate(R.layout.simple_confirm_dialog, null)
        val builder = AlertDialog.Builder(context)
        builder.setView(view)
        val dialog = builder.create()
        view.simpleDialogTitle.text = property.title
        view.simpleDialogYes.text = property.positiveText
        view.simpleDialogNo.text = property.negativeText
        view.simpleDialogYesContainer.setOnClickListener {listener.onButtonClicked(DialogInterface.BUTTON_POSITIVE); dialog.dismiss() }
        view.simpleDialogNoContainer.setOnClickListener { dialog.dismiss()}
        if(showEditText){
            view.simpleDialogInputCardView.visibility = View.VISIBLE
        }else{
            view.simpleDialogInputCardView.visibility = View.GONE
        }

        clickEffectGrey(context,view.simpleDialogNoContainer,true)
        clickEffectGrey(context,view.simpleDialogYesContainer,false)
        val inset = InsetDrawable(ColorDrawable(Color.TRANSPARENT),100,0,100,0)
        dialog.window?.setBackgroundDrawable(inset)
        dialog.show()
    }
}