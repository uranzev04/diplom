package com.example.diploma.commonUtils

import android.content.Context
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.diploma.R
/**
 *Created By Uranzev
 * 2019
 */
    fun showCircleImage(image:Any, imageView: AppCompatImageView, placeHolder:Int = R.drawable.ic_dark_profile, context:Context){
        Glide.with(context).load(image).apply(RequestOptions().placeholder(placeHolder)).apply(
            RequestOptions.circleCropTransform()).into(imageView)
    }
    fun showRoundedImage(image:Any, imageView:AppCompatImageView, placeHolder:Int = R.drawable.ic_dark_profile,context: Context){
        Glide.with(context).setDefaultRequestOptions(RequestOptions().placeholder(placeHolder).transforms(
            RoundedCorners(12)
        )).load(image).into(imageView)
    }

    fun clickEffectGrey(context: Context,view: View, isLeft:Boolean){
        view.setOnTouchListener { v, event ->
            when(event.action){
                MotionEvent.ACTION_DOWN ->{
                    if(isLeft)
                        v.background = ContextCompat.getDrawable(context,R.drawable.click_effect_left_bottom_drawable)
                    else
                        v.background = ContextCompat.getDrawable(context,R.drawable.click_effect_right_bottom_drawable)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP ->{
                    v.setBackgroundColor(ContextCompat.getColor(context, R.color.colorTransparent))
                    v.invalidate()
                }
        }
        false
     }

    }

    fun formatTimeWithMinuteHour(seconds: Int): String {
        val totalMinute = seconds / 60
        val secondText = String.format("%02d", seconds % 60)
        val minuteText = String.format("%02d",  totalMinute)

        return  "$minuteText:$secondText"
    }