package com.example.diploma

enum class AnswerResult{
    CORRECT, WRONG, SKIPPED
}
enum class ExamTypeEnum{
    AP, FE, IP
}
enum class QuestionField{
    technology, strategy, management
}

