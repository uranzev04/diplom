package com.example.diploma.login

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.diploma.MainActivity
import com.example.diploma.R
import com.example.diploma.SPManager
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class LoginActivity : AppCompatActivity() {
    private var RC_SIGN_IN = 37
    private lateinit var auth: FirebaseAuth
    private var TAG = "login"
    private lateinit var db: FirebaseFirestore
    private val loginFragmet = LoginFragment1()
    private val signinFragment = SignInFragment()
    private val signUpFragment = SignUpFragment()
    private val recoverPasswordFrament = RecoverPasswordFragment()
    private val newPasswordFragment = NewPasswordFragment()
    private val recoverPasswordFragment = RecoverPasswordFragment()
    private val enterVerificationFragment = EnterVerificationFragment()
    private lateinit var callBack:CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.diploma.R.layout.activity_login)



        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        callBack = CallbackManager.Factory.create()



        buttonSignIn.setOnClickListener {
                    changeFragment(signinFragment,signinFragment::class.java.name)
        }
        buttonSignUp.setOnClickListener {
            Log.e(TAG,"sign up")
                    changeFragment(signUpFragment,signUpFragment::class.java.name)

        }

        facebookButton.setOnClickListener {
            facebookLogin()
        }
        googleButton.setOnClickListener {
            googleLogin()
        }
    }
     fun showLoading(){
        loadingScreen.isClickable = true
        loadingScreen.isFocusable = true
        loginProgressBar.visibility = View.VISIBLE
    }
     fun hideLoading(){
        loadingScreen.isClickable = false
        loadingScreen.isFocusable = false
        loginProgressBar.visibility = View.GONE
    }
    private fun changeFragment(fragment: Fragment, name: String) {
        supportFragmentManager.beginTransaction().replace(R.id.loginContainer, fragment).addToBackStack(name).commit()
    }

    private fun resetLoginAcitivity(){
        finish()
        startActivity(intent)
    }
    fun googleLogin(){
        showLoading()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(com.example.diploma.R.string.default_web_cliend_id))
            .requestEmail()
            .build()
       val  mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callBack.onActivityResult(requestCode, resultCode, data)
        if(requestCode == RC_SIGN_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account!!)
            }catch (e:Exception){

            }

        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.id!!)

        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.e(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user!!)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e(TAG, "signInWithCredential:failure", task.exception)
                    Snackbar.make(loginContainer, "Authentication Failed.",
                        Snackbar.LENGTH_SHORT).show()
                    updateUI(null)
                }
                hideLoading()
                // ...
            }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        Log.e(TAG,"onStart")
        updateUI(currentUser)
    }

    private fun updateUI(user:FirebaseUser?){
        if(user != null){
            Log.e(TAG,"name =>${user.displayName}")
            Log.e(TAG,"mail =>${user.email}")
            Log.e(TAG,"url =>${user.uid}")
            SPManager(this).setUserUid(user.uid)
            if(user.displayName != null)SPManager(this).setUserName(user.displayName!!)
            SPManager(this).setUserAvatar(user.photoUrl.toString())
            callMainActivity()
        }
    }
     fun createUser(email: String, password:String, name:String){
        auth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if(it.exception is FirebaseAuthUserCollisionException){
                    Toast.makeText(this,getString(R.string.emailCollision), Toast.LENGTH_SHORT).show()
                }
            }
            .addOnSuccessListener {
                Log.e(TAG,"user create success")
                saveUser(name)
            }
            .addOnFailureListener {
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            }
    }


    private fun saveUser(name:String){
       val user = auth.currentUser
        val profileUpdates = UserProfileChangeRequest.Builder()
            .setDisplayName(name)
            .build()
        user?.updateProfile(profileUpdates)?.addOnCompleteListener {
            if(it.isSuccessful)
                Log.e(TAG,"name uploaded")
            hideLoading()
             }
            ?.addOnFailureListener {
                Log.e(TAG,"name error =>$it")
            }
            ?.addOnSuccessListener {
                resetLoginAcitivity()
            }
    }
    fun signIn(email:String,password: String){
        auth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                Log.e(TAG,"sign in completed")
                hideLoading()
            }
            .addOnFailureListener {
                Log.e(TAG,"sign in error =>$it")
                Toast.makeText(this,getString(R.string.error_occured),Toast.LENGTH_LONG).show()
            }
            .addOnSuccessListener {
                val user = auth.currentUser
                updateUI(user)
            }
    }
    fun facebookLogin(){
        LoginManager.getInstance().registerCallback(callBack,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) { // App code
                    Log.e(TAG,"fb login success")
                    handleFacebookAccessToken(loginResult!!.accessToken)
                }

                override fun onCancel() { // App code
                }

                override fun onError(exception: FacebookException) { //
                    Log.e(TAG,"fb error =>$exception")
                }
            })
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email","public_profile"))
    }
    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(TAG, "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
                // ...
            }
    }

    private fun printHashKey(){
        try {
            val info: PackageInfo = this.packageManager.getPackageInfo(this.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(),0))
                Log.i(TAG, "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "printHashKey()", e)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
    fun showRecoverPassword(){
        changeFragment(recoverPasswordFrament,recoverPasswordFrament::class.java.name)
    }
    fun showEnterVerifcation(){
        changeFragment(enterVerificationFragment, enterVerificationFragment::class.java.name)
    }
    fun showNewPassword(){
        changeFragment(newPasswordFragment, newPasswordFragment::class.java.name)
    }

    fun callMainActivity(){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
