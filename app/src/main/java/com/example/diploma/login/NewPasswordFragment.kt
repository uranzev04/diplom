package com.example.diploma.login


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.diploma.R
import kotlinx.android.synthetic.main.fragment_new_password.*

/**
 * A simple [Fragment] subclass.
 */
class NewPasswordFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newPasswordBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        updatePasswordButton.setOnClickListener {
            //TODO call main from here
        }
    }

}
