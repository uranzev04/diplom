package com.example.diploma.login


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.diploma.R
import kotlinx.android.synthetic.main.fragment_recover_password.*

/**
 * A simple [Fragment] subclass.
 */
class RecoverPasswordFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recover_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recoverPasswordBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        buttonRecoverPassword.setOnClickListener {
            (activity as LoginActivity).showEnterVerifcation()
        }
    }


}
