package com.example.diploma.login


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.diploma.R
import kotlinx.android.synthetic.main.fragment_sign_up.*

/**
 * A simple [Fragment] subclass.
 */
class SignUpFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }
    private var name = ""
    private var email = ""
    private var password = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signUpBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        buttonSignUp.setOnClickListener {
            name = signUpNameEditText.text.toString()
            email = signUpMailEditText.text.toString()
            password = signUpPasswordEditText.text.toString()
            signUp()
        }
    }

    private fun signUp(){
        if(name.isNotEmpty() && email.isNotEmpty() && password.length > 6){
            Log.e("up","not empty")
            (activity as LoginActivity).showLoading()
            (activity as LoginActivity).createUser(email,password,name)
        }else{
            Toast.makeText(activity!!, getString(R.string.unknown_error), Toast.LENGTH_SHORT).show()
        }
    }
}
