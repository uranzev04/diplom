package com.example.diploma.login


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.diploma.R
import kotlinx.android.synthetic.main.fragment_sign_in.*

/**
 * A simple [Fragment] subclass.
 */
class SignInFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }
    private var email = ""
    private var password = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signInBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        forgotPassword.setOnClickListener {
            (activity as LoginActivity).showRecoverPassword()
        }
        facebookButtonSignIn.setOnClickListener {
            (activity as LoginActivity).facebookLogin()
        }
        googleButtonSignIn.setOnClickListener {
            (activity as LoginActivity).googleLogin()
        }
        buttonSignIn.setOnClickListener {
            email = signinMailEditText.text.toString()
            password = siginPasswordEditText.text.toString()
            signIn()
        }

    }

    private fun signIn(){
        if(email.isNotEmpty() && password.length >= 6){
            (activity as LoginActivity).showLoading()
            (activity as LoginActivity).signIn(email,password)
        }
        else
            Toast.makeText(activity!!,getString(R.string.unknown_error),Toast.LENGTH_SHORT).show()
    }


}
