package com.example.diploma
/**
 *Created By Uranzev
 * 2019
 */
data class Questions(var question:String = "",
                     var answer1:String = "",
                     var answer2:String ="",
                     var answer3:String = "",
                     var answer4:String = "",
                     var correctAnswer:Int = 0,
                     var isAnswersImage:Boolean = false,
                     var subQuestion:ArrayList<String>? = null,
                     var additionals:ArrayList<String>? = null,
                     var year:Int = 0,
                     var category:String = "",
                     var am:Boolean = false,
                     var examType:String = "",
                     var point:Int = 0,
                     var field:String = "")

data class ExamType(var type:String = "",
                    var name:String = "",
                    var description:String = "",
                    var isAm:Int = 0,
                    var durationTime:Int = 0,
                    var totalQuestion:Int = 0)
data class User(var Name:String ="",
                var email:String = "",
                var profile:String ="",
                var password:String = "",
                var isSocial:Boolean = false)
data class ConfirmDialogProperty(var positiveText:String = "",
                                 var negativeText:String = "",
                                 var error:String = "",
                                 var title:String = "")
data class Answers(var answer:String ="",
                   var isImageOrNot:Int = 0)
data class SelectedAnswers(var questionNumber:Int = 0,
                           var questionAnswer:Int = -1)
data class CorrectAnswers(var questionNumber:Int = 0,
                          var answerResult:String = "")
data class AnswerResultItem(var question:String ="",
                        var answerResult: String ="",
                        var correctAnswer:String = "",
                            var isAnswerImage:Boolean = false)
data class History(var date:Long = 0,
                   var receivedPoint:Int = 0,
                   var am:Int = 0,
                   var examType:String = "",
                   var uid:String = "",
                   var correctAnswer:Int = 0,
                   var totalAnswer:Int = 0)
data class LeaderBoardPoint(var uid:String = "",
                            var name:String = "",
                            var feamPoint:Int = 0,
                            var fepmPoint:Int = 0,
                            var apamPoint:Int = 0,
                            var appmPoint:Int = 0,
                            var ippoint:Int = 0,
                            var photoUrl:String = "")
data class LeaderBoardTypePoint(var point:Int = 0,
                                var url:String = "",
                                var name:String = "")

