package com.example.diploma.adapters

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.diploma.fragments.StatisticItemFragment

class StatisticItemAdapter(fm:FragmentManager, list1:List<Float>,list2:List<Float>):FragmentPagerAdapter(fm) {
    private val fragments = arrayListOf(StatisticItemFragment("pie",list1),
        StatisticItemFragment("column",list2))
    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}