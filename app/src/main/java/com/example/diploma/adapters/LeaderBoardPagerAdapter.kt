package com.example.diploma.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.diploma.LeaderBoardPoint
import com.example.diploma.fragments.LeaderBoardPointTypeFragment

class LeaderBoardPagerAdapter(fm:FragmentManager,leaderBoards:ArrayList<LeaderBoardPoint> ):FragmentPagerAdapter(fm) {
    private val fragments = arrayListOf(LeaderBoardPointTypeFragment("apamPoint", leaderBoards),
        LeaderBoardPointTypeFragment("appmPoint",leaderBoards),
        LeaderBoardPointTypeFragment("fepmPoint",leaderBoards),
        LeaderBoardPointTypeFragment("feamPoint",leaderBoards),
        LeaderBoardPointTypeFragment("ippoint",leaderBoards))
    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

}