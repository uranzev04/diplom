package com.example.diploma.adapters

import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.example.diploma.ExamType
import com.example.diploma.fragments.ExamTypListFragment

class MainViewPagerAdapter(fm:FragmentManager, examTypes:ArrayList<ExamType>):FragmentPagerAdapter(fm) {


    private val fragments = arrayListOf(ExamTypListFragment(examTypes,false), ExamTypListFragment(examTypes,true))

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }


    override fun getCount(): Int {
        return fragments.size
    }
}