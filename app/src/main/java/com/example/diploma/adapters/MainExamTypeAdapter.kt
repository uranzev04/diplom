package com.example.diploma.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.diploma.ExamType
import com.example.diploma.R
import com.example.diploma.interfaces.RecyclerItemClickListener
import kotlinx.android.synthetic.main.main_recycler_item.view.*
/**
 *Created By Uranzev
 * 2019
 */
class MainExamTypeAdapter(var listener:RecyclerItemClickListener, val isEnglish:Boolean):RecyclerView.Adapter<MainExamTypeAdapter.MainExamTypeAdapterViewHolder>() {
    var examTypes = ArrayList<ExamType>()
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainExamTypeAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.main_recycler_item, parent, false)
        context= parent.context
        return MainExamTypeAdapterViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return examTypes.size
    }

    override fun onBindViewHolder(holder: MainExamTypeAdapterViewHolder, position: Int) {
        val itemView = holder.itemView
        val examType = examTypes[position]
        when(examType.type){
            "IP"->{
                itemView.examTypeType.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_ip_blue)
            }
            "FE" ->{
                itemView.examTypeType.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_fe_brown)

            }
            "AP"->{
                itemView.examTypeType.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_ap_red)
            }
        }
        Log.e("adapter","data =>${examType}")
        itemView.examTypeName.text = examType.name
        itemView.examTypeType.text = examType.type
        itemView.examDuration.text = context.resources.getString(R.string.duration) + examType.durationTime.toString() + context.resources.getString(R.string.minute)
        itemView.examLanguage.text = itemView.context.getString( if(isEnglish) R.string.english else R.string.mongol)
        when(examType.isAm){
            1 ->{    itemView.examAm.text = context.resources.getString(R.string.morning)}
            2 ->{   itemView.examAm.text = context.resources.getString(R.string.day) }
            else ->{   itemView.examAm.visibility = View.GONE }
        }
        itemView.examTotalQuestion.text = context.resources.getString(R.string.questionCount)+ examType.totalQuestion.toString()
        itemView.examTypeButton.setOnClickListener { listener.onItemClickListener(position,examType) }
    }

    fun setDatas(types:ArrayList<ExamType>){
        examTypes.clear()
        examTypes.addAll(types)
    }
    class MainExamTypeAdapterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
}