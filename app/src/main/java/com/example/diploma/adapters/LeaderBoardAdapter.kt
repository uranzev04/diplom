package com.example.diploma.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diploma.LeaderBoardTypePoint
import com.example.diploma.R
import com.example.diploma.commonUtils.showCircleImage
import kotlinx.android.synthetic.main.leader_board_adapter_item.view.*

class LeaderBoardAdapter():RecyclerView.Adapter<LeaderBoardAdapter.LeaderBoardAdapterViewHolder>() {
    class LeaderBoardAdapterViewHolder(itemView:View):RecyclerView.ViewHolder(itemView)
    var points = ArrayList<LeaderBoardTypePoint>()
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderBoardAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.leader_board_adapter_item, parent, false)
        context = parent.context
        return LeaderBoardAdapterViewHolder(itemView)
    }

    override fun getItemCount(): Int {
       return points.size
    }

    override fun onBindViewHolder(holder: LeaderBoardAdapterViewHolder, position: Int) {
        val itemView = holder.itemView
        val point = points[position]
        itemView.leaderBoardItemNo.text = (position+1).toString()+"."
        itemView.leaderBoardItemName.text = point.name
        itemView.leaderBoardTotalPoint.text = context.resources.getString(R.string.point)+point.point
        showCircleImage(point.url, itemView.leaderBoardItemImage,R.drawable.ic_dark_profile,context)
    }
    fun setData(data:ArrayList<LeaderBoardTypePoint>){
        points.clear()
        points.addAll(data)
        points.sortByDescending { it.point }
        notifyDataSetChanged()
    }
}