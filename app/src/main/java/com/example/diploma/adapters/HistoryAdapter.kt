package com.example.diploma.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.diploma.History
import com.example.diploma.R
import kotlinx.android.synthetic.main.history_adapter_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryAdapter():RecyclerView.Adapter<HistoryAdapter.HistoryAdapterViewHolder>() {
    class HistoryAdapterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    var histories = ArrayList<History>()
    lateinit  var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.history_adapter_item, parent,false)
        context = parent.context
        return HistoryAdapterViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return  histories.size
    }

    override fun onBindViewHolder(holder: HistoryAdapterViewHolder, position: Int) {
        val itemView = holder.itemView
        val history = histories[position]
        val format = SimpleDateFormat("yyyy-MM-dd hh:mm").format(Date(history.date)).toString()
        itemView.historyItemDate.text = format
        when(history.examType){
            "IP"->{
                itemView.historyItemTypeContainer.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_ip_blue)
            }
            "FE" ->{
                itemView.historyItemTypeContainer.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_fe_brown)

            }
            "AP"->{
                itemView.historyItemTypeContainer.background = ContextCompat.getDrawable(context,R.drawable.fill_rounded_ap_red)
            }
        }
        itemView.historyItemCorrectAnswer.text = history.correctAnswer.toString() + "/"+history.totalAnswer
        itemView.historyItemReceivedPoints.text = history.receivedPoint.toString()
        itemView.historyItemType.text = history.examType

    }
    fun setData(data:ArrayList<History>){
        histories.clear()
        histories.addAll(data)
    }
}