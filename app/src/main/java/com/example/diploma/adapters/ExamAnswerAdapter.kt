package com.example.diploma.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.diploma.Answers
import com.example.diploma.Questions
import com.example.diploma.R
import com.example.diploma.commonUtils.showRoundedImage
import com.example.diploma.interfaces.RecyclerItemClickListener
import kotlinx.android.synthetic.main.exam_answer_item.view.*

class ExamAnswerAdapter(var listener:RecyclerItemClickListener):RecyclerView.Adapter<ExamAnswerAdapter.ExamAnswerAdapterViewHolder>() {
    class ExamAnswerAdapterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    var question = Questions()
    var list = listOf<String>()
    var viewList = ArrayList<View>()
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamAnswerAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.exam_answer_item,parent,false)
        val viewHolder = ExamAnswerAdapterViewHolder(itemView)
        context = parent.context
        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ExamAnswerAdapterViewHolder, position: Int) {
        val itemView = holder.itemView
        if(!viewList.contains(itemView))
            viewList.add(itemView)
        val answer = list[position]
        if(question.isAnswersImage){
            itemView.answerItemText.visibility = View.GONE
            showRoundedImage(answer,itemView.answerItemImage,context = context)
        }else{
            itemView.answerItemImage.visibility = View.GONE
            itemView.answerItemText.text = answer
        }
        itemView.answerItem.setOnClickListener {
            listener.onItemClickListener(position,answer)
            for( item in  viewList){
                item.answerItem.background = ContextCompat.getDrawable(context,R.drawable.search_address_input_bg)
            }
            itemView.answerItem.background = ContextCompat.getDrawable(context,R.drawable.rounded_stroke)

        }
    }


    fun setData(data:Questions){
        question = data
         list = listOf(question.answer1, question.answer2, question.answer3, question.answer4)
    }
}