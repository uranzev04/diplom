package com.example.diploma.adapters

import android.content.Context
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diploma.AnswerResult
import com.example.diploma.AnswerResultItem
import com.example.diploma.R
import com.example.diploma.commonUtils.showRoundedImage
import com.example.diploma.interfaces.RecyclerItemClickListener
import kotlinx.android.synthetic.main.check_answers_adapter_item.view.*

class CheckAnswersAdapter(var listener:RecyclerItemClickListener):RecyclerView.Adapter<CheckAnswersAdapter.CheckAnswersAdapterViewHolder>() {
    class CheckAnswersAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    private lateinit var context: Context
    var answerResults = ArrayList<AnswerResultItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckAnswersAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.check_answers_adapter_item,parent,false)
        context = parent.context
        return CheckAnswersAdapterViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return answerResults.size
    }
    override fun onBindViewHolder(holder: CheckAnswersAdapterViewHolder, position: Int) {
        val itemView = holder.itemView
        val data = answerResults[position]
        when(data.answerResult){
            AnswerResult.SKIPPED.toString() ->{
                itemView.checkAnswersItemIcon.setImageResource(R.drawable.circle_grey)
            }
            AnswerResult.WRONG.toString() ->{
                itemView.checkAnswersItemIcon.setImageResource(R.drawable.ic_close_black_24dp)
            }
            AnswerResult.CORRECT.toString() ->{
                itemView.checkAnswersItemIcon.setImageResource(R.drawable.ic_done_black_24dp)
            }
        }
        itemView.checkAnswersItemQuestion.text = data.question
        itemView.checkAnswersItemNo.text = (position+1).toString()

        if(data.isAnswerImage){
            showRoundedImage(data.correctAnswer,itemView.checkAnswersItemImage,R.drawable.ic_close_black_24dp,context)
            itemView.checkAnswersItemCorrectAnswer.visibility = View.GONE
        }else{
            itemView.checkAnswersItemCorrectAnswer.text = data.correctAnswer
            itemView.checkAnswersItemImage.visibility = View.GONE
        }
        itemView.checkAnswersItem.setOnClickListener {
            if(itemView.checkAnswersBottomSection.visibility == View.VISIBLE){
                itemView.checkAnswersBottomSection.visibility = View.GONE
            }else{
                itemView.checkAnswersBottomSection.visibility = View.VISIBLE
            }
            listener.onItemClickListener(position,data)
        }
    }

    fun setData(data:ArrayList<AnswerResultItem>){
        answerResults.clear()
        answerResults = data
    }
}