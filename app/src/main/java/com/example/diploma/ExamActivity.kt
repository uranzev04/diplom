package com.example.diploma

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diploma.adapters.ExamAnswerAdapter
import com.example.diploma.commonUtils.CommonDialog
import com.example.diploma.commonUtils.formatTimeWithMinuteHour
import com.example.diploma.commonUtils.showRoundedImage
import com.example.diploma.fragments.CheckAnswersFragment
import com.example.diploma.fragments.HistoryFragment
import com.example.diploma.interfaces.DialogButtonClickListener
import com.example.diploma.interfaces.RecyclerItemClickListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_exam.*
import kotlinx.android.synthetic.main.main_recycler_item.*
import kotlinx.android.synthetic.main.question_additionals.view.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

/**
 *Created By Uranzev
 * 2019
 */
class ExamActivity : AppCompatActivity(){


    private lateinit var db: FirebaseFirestore
    private lateinit var questions:ArrayList<Questions>
    private lateinit var adapter:ExamAnswerAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private var correctAnswersCount = 0
    private var page = 0
    private var selectedAnswerNumber = -1
    private var type = ""
    private var isAm = 0
    private var totalQuestions = 0
    private var selectedAnswers = ArrayList<SelectedAnswers>()
    private var correctAnswers = ArrayList<SelectedAnswers>()
    private var checkAnswers = ArrayList<CorrectAnswers>()
    private val checkAnswersFragmet = CheckAnswersFragment()
    private var points = 0
    private var time = 0
    private var isExam = true
    private val timeHandler = Handler()
    private val timeRunner = object :Runnable{
        override fun run() {
            if(time>0){
                examTimeText.text = formatTimeWithMinuteHour(time)
                time -= 1
                timeHandler.postDelayed(this,1000)
            }else {
                stopTimer()
                Toast.makeText(this@ExamActivity,getString(R.string.timeFinished),Toast.LENGTH_LONG).show()
                finish()
            }
        }

    }

    private fun stopTimer(){
        try {
            timeHandler.removeCallbacksAndMessages(null)
        }catch (e:Exception){e.printStackTrace()}
    }

    private fun startTimer(){
        stopTimer()
        timeHandler.postDelayed(timeRunner,0)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopTimer()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam)
        db = FirebaseFirestore.getInstance()
        if(intent.getStringExtra("type") != null){
            type = intent.getStringExtra("type")
        }
        isAm = intent.getIntExtra("isAm",0)
        time = intent.getIntExtra("time",0)*60
        totalQuestions = intent.getIntExtra("totalQuestion",0)
        isExam = intent.getBooleanExtra("exam",true)
        if(isExam)
            getTechnologyQuestions()
        else
            getExerciseQuestions()
        examContinueButton.setOnClickListener { nextPage() }
        examHeaderImage.setOnClickListener {
            val property = ConfirmDialogProperty()
            property.title = if(SPManager(this@ExamActivity).getExerciseYear() == 0) getString(R.string.wantToQuitExam) else getString(R.string.wantToquitExercise)
            property.positiveText = getString(R.string.yes)
            property.negativeText = getString(R.string.no)
            CommonDialog.showConfirmDialog(this,property, object: DialogButtonClickListener {
                override fun onButtonClicked(buttonType: Int) {
                    if(buttonType == DialogInterface.BUTTON_POSITIVE){
                        if(SPManager(this@ExamActivity).getExerciseYear() == 0)
                            Toast.makeText(this@ExamActivity,getString(R.string.quit_exam),Toast.LENGTH_SHORT).show()
                        else
                            Toast.makeText(this@ExamActivity,getString(R.string.quitExercise),Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            })

        }



//        changeAnswersBackground()
        getLeaderBoard()
    }



    private fun setAnswers(){
        adapter = ExamAnswerAdapter(object :RecyclerItemClickListener{
            override fun onItemClickListener(position: Int, data: Any) {
                Log.e("exam","item position =>${position+1}")
                selectedAnswerNumber = position+1
            }

        })
        adapter.setData(questions[page])
        layoutManager = LinearLayoutManager(this)
        examRecyclerView.layoutManager = layoutManager
        examRecyclerView.adapter = adapter
    }
    private fun nextPage(){
        selectedAnswers[page].questionAnswer = selectedAnswerNumber
        if(page == questions.size -2){
            examContinueButton.text = getString(R.string.finish)
        }
        if(page == questions.size -1){
           finishExam()
        }else{
            page = page.inc()
            setDatas()
        }

    }

    private fun finishExam(){
        stopTimer()
        for(i in 0 until correctAnswers.size){
            Log.e("exam","correctAnswersAnswer =>${correctAnswers[i].questionAnswer}")
            Log.e("exam","correctAnswersAnswer1 =>${selectedAnswers[i].questionAnswer}")
            val correctAnswer = CorrectAnswers()
            correctAnswer.questionNumber = i
            if(correctAnswers[i].questionAnswer == selectedAnswers[i].questionAnswer){
                correctAnswer.answerResult = AnswerResult.CORRECT.toString()
            }else{
                if( selectedAnswers[i].questionAnswer == -1) correctAnswer.answerResult = AnswerResult.SKIPPED.toString()
                else correctAnswer.answerResult = AnswerResult.WRONG.toString()

            }

            checkAnswers.add(correctAnswer)

        }
        Log.e("exam","checkAnswers =>${checkAnswers.size}")
        var count = 0
        var skipCount = 0
        val answerResults = ArrayList<AnswerResultItem>()

        for(i in  0 until checkAnswers.size){
            val result = AnswerResultItem()
            Log.e("exam","checkAnswers =>${checkAnswers[i]}")
            when (checkAnswers[i].answerResult) {
                AnswerResult.CORRECT.toString() -> {
                    count = count.inc()
                    points += questions[i].point
                    result.answerResult = AnswerResult.CORRECT.toString()
                }
                AnswerResult.SKIPPED.toString() -> {
                    skipCount = skipCount.inc()
                    result.answerResult = AnswerResult.SKIPPED.toString()
                }
                AnswerResult.WRONG.toString() -> {
                    result.answerResult = AnswerResult.WRONG.toString()
                }
            }
            result.isAnswerImage = questions[i].isAnswersImage
            result.correctAnswer = when(questions[i].correctAnswer){
                1 ->{
                    questions[i].answer1
                }
                2 ->{
                    questions[i].answer2
                }
                3 ->{
                    questions[i].answer3
                }
                4 ->{
                    questions[i].answer4
                }
                else ->{""}
            }
            result.question  = questions[i].question
            answerResults.add(result)

        }
        checkAnswersFragmet.answerResults = answerResults
        checkAnswersFragmet.correctAnswersCount =count
        checkAnswersFragmet.receivedPoints = points
        correctAnswersCount = count
        checkAnswersFragmet.skippedAnswersCount = skipCount
        checkAnswersFragmet.wrongAnswersCount = questions.size - (count+skipCount)
        checkAnswersFragmet.questions = questions
        Log.e("check","results =>${answerResults[4].question}")
        showCheckAnswersFragment()
    }

    private fun selectPage(number:Int){
        page = number
        setDatas()
    }
    var number = 0

    private fun getExerciseQuestions(){
        Log.e("exercise","field =>${SPManager(this).getExerciseField()}")
        Log.e("exercise","examType =>${SPManager(this).getExerciseType()}")
        Log.e("exercise","year =>${SPManager(this).getExerciseYear()}")
        Log.e("exercise","am =>${SPManager(this).getExerciseIsam()}")
            db.collection("questions")
                .whereEqualTo("field",SPManager(this).getExerciseField())
                .whereEqualTo("examType",SPManager(this).getExerciseType())
                .whereEqualTo("am",!SPManager(this).getExerciseIsam())
                .whereEqualTo("year",SPManager(this).getExerciseYear())
                .limit(20)
                .get()
                .addOnSuccessListener {
                    questions = ArrayList()
                    questions.clear()
                    for(document in it){
                        Log.e("exercise", "${document.id} ==> ${document.data}")

                        val quest = Gson().fromJson(Gson().toJson(document.data),Questions::class.java)
                        val correctAnswer = SelectedAnswers()
                        val selectedAnswer = SelectedAnswers()
                        correctAnswer.questionNumber = number
                        Log.e("exercise","number = >$number")
                        correctAnswer.questionAnswer = quest.correctAnswer
                        correctAnswers.add(correctAnswer)
                        selectedAnswer.questionNumber = number
                        selectedAnswers.add(selectedAnswer)

                        Log.e("exercise","boolean =>${quest.isAnswersImage}")


                        number = number.inc()
                        questions.add(quest)
                    }
                    setDatas()
                    initSpinner()
                }
                .addOnCompleteListener {  }
                .addOnFailureListener {

                }

    }


    private fun getTechnologyQuestions(){
        db.collection("questions")
            .whereEqualTo("field","technology")
            .whereEqualTo("examType",type)
            .whereEqualTo("year",2016)
            .limit(50)
            .get()
            .addOnSuccessListener {
                questions = ArrayList()
                questions.clear()
                for(document in it){
                    Log.e("exam", "${document.id} ==> ${document.data}")

                    val quest = Gson().fromJson(Gson().toJson(document.data),Questions::class.java)
                    val correctAnswer = SelectedAnswers()
                    val selectedAnswer = SelectedAnswers()
                    correctAnswer.questionNumber = number
                    Log.e("exam","number = >$number")
                    correctAnswer.questionAnswer = quest.correctAnswer
                    correctAnswers.add(correctAnswer)
                    selectedAnswer.questionNumber = number
                    selectedAnswers.add(selectedAnswer)

                    Log.e("exam","boolean =>${quest.isAnswersImage}")


                    number = number.inc()
                    questions.add(quest)
                }
                getManagementQuestions()

            }
            .addOnFailureListener {
                Log.e("exam","error")
            }
            .addOnCompleteListener {
                Log.e("exam","complete exam")
            }
    }

    private fun getManagementQuestions(){
        db.collection("questions")
            .whereEqualTo("examType",type)
            .whereEqualTo("field","management")
            .limit(10)
            .get()
            .addOnSuccessListener {
                for(document in it){
                    val quest = Gson().fromJson(Gson().toJson(document.data),Questions::class.java)
                    val correctAnswer = SelectedAnswers()
                    val selectedAnswer = SelectedAnswers()
                    correctAnswer.questionNumber = number
                    Log.e("exam","number = >$number")
                    correctAnswer.questionAnswer = quest.correctAnswer
                    correctAnswers.add(correctAnswer)
                    selectedAnswer.questionNumber = number
                    selectedAnswers.add(selectedAnswer)

                    Log.e("exam","boolean =>${quest.isAnswersImage}")


                    number = number.inc()
                    questions.add(quest)
                }
                getStrategyQuestions()
            }
            .addOnCompleteListener {
                Log.e("exam","error")

            }
            .addOnFailureListener {
                Log.e("exam","complete exam")
            }
    }
    private fun getStrategyQuestions(){
        db.collection("questions")
            .whereEqualTo("examType",type)
            .whereEqualTo("field","strategy")
            .limit(20)
            .get()
            .addOnSuccessListener {
                for(document in it){
                    val quest = Gson().fromJson(Gson().toJson(document.data),Questions::class.java)
                    val correctAnswer = SelectedAnswers()
                    val selectedAnswer = SelectedAnswers()
                    correctAnswer.questionNumber = number
                    Log.e("exam","number = >$number")
                    correctAnswer.questionAnswer = quest.correctAnswer
                    correctAnswers.add(correctAnswer)
                    selectedAnswer.questionNumber = number
                    selectedAnswers.add(selectedAnswer)

                    Log.e("exam","boolean =>${quest.isAnswersImage}")
                    number = number.inc()
                    questions.add(quest)
                }
                setDatas()
                initSpinner()
            }
            .addOnCompleteListener {
                Log.e("exam","error")

            }
            .addOnFailureListener {
                Log.e("exam","complete exam")
            }
    }

    private fun setDatas(){
        if(questions.size > 0){
            examNoQuestionText.visibility = View.GONE
            examQuestionsContainer.visibility = View.VISIBLE
            if(SPManager(this).getExerciseYear() == 0) startTimer()
            examQuestion.text = questions[page].question
            Log.e("exam","isAnswersImage =>${questions[page].isAnswersImage}")
            Log.e("exam","isAnswersImage =>${questions[page]}")
            Log.e("exam","isAnswersImage =>$page")
            additionalContainer.removeAllViews()
            if(questions[page].additionals != null){
                Log.e("exam","additionals =>${questions[page].additionals}")


//                for(s in questions[page].additionals!!){
//                    if(s.startsWith("http")){
//                        showRoundedImage(s,view.additionalImage,context = this)
//                        view.additionalTextContainer.visibility = View.GONE
//                    }else{
//                        view.additionalImage.visibility  = View.GONE
//                        view.additionalText.text = s
//                    }
//                }
                questions[page].additionals!!.mapIndexed { index, s ->
                    val view = LayoutInflater.from(this).inflate(R.layout.question_additionals,null)
                    if(s.startsWith("http")){
                        showRoundedImage(s,view.additionalImage,context = this)
                        view.additionalTextContainer.visibility = View.GONE
                    }else{
                        view.additionalImage.visibility  = View.GONE
                        view.additionalText.text = s
                    }
                    additionalContainer.addView(view)

                }



            }
            setAnswers()
            examHeaderText.text = (page+1).toString() + "/" +questions.size
        }else{
            examNoQuestionText.visibility = View.VISIBLE

            examQuestionsContainer.visibility = View.GONE
            Toast.makeText(this,getString(R.string.noQuestions), Toast.LENGTH_SHORT).show()
        }


    }
    private fun initSpinner(){
        val pageList = ArrayList<Int>()
        for(i in 0 until questions.size){
            pageList.add(i+1)
        }
        examPageSpinner.adapter = ArrayAdapter<Int>(this,android.R.layout.simple_list_item_1, pageList)
        examPageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectPage(position)
                Log.e("exam","position =>$position")
                (view as TextView).text = null
            }

        }
    }

    private fun showCheckAnswersFragment(){
        supportFragmentManager.beginTransaction().replace(R.id.examContainer, checkAnswersFragmet).addToBackStack(checkAnswersFragmet::class.java.name).commit()
    }
    fun finishActivity(){
        finish()
    }

     fun uploadHistory(){
        val history  = History()
        history.date = Calendar.getInstance().timeInMillis
        history.examType = type
        history.am = isAm
        history.uid = SPManager(this).getUserUid()
        history.correctAnswer = correctAnswersCount
        history.totalAnswer = totalQuestions
        history.receivedPoint = points
        db.collection("history").add(history).addOnSuccessListener {
            Log.e("history","success")

            }
            .addOnFailureListener {
                Log.e("history","error=>${it}")
            }
            .addOnCompleteListener {
                uploadLeaderBoard()
            }

    }
    fun uploadLeaderBoard(){
        val leaderBoardPoint = LeaderBoardPoint()
        leaderBoardPoint.name = SPManager(this).getUserName()
        leaderBoardPoint.uid = SPManager(this).getUserUid()
        leaderBoardPoint.photoUrl = SPManager(this).getUserAvatar()
        var isExceededMaxPoint = false
        Log.e("leader","leader =>$leaderBoard")
        var data = hashMapOf("" to 0)
        when(type){
            ExamTypeEnum.AP.toString() ->{
                when(isAm){
                    1 ->{
                        if(leaderBoard.apamPoint < points) leaderBoardPoint.apamPoint = points else leaderBoardPoint.apamPoint = leaderBoard.apamPoint
                        isExceededMaxPoint = true
                        data = hashMapOf("apamPoint" to points)
                    }
                    2 ->{if(leaderBoard.appmPoint < points) leaderBoardPoint.appmPoint = points else leaderBoardPoint.appmPoint = leaderBoard.appmPoint
                        isExceededMaxPoint = true
                        data = hashMapOf("appmPoint" to points)}

                }
            }
            ExamTypeEnum.FE.toString() ->{
                when(isAm){
                    1 ->{if(leaderBoard.feamPoint < points) leaderBoardPoint.feamPoint = points else leaderBoardPoint.feamPoint = leaderBoard.feamPoint
                        isExceededMaxPoint = true
                        data = hashMapOf("feamPoint" to points)}
                    2 ->{if(leaderBoard.fepmPoint < points) leaderBoardPoint.fepmPoint = points else leaderBoardPoint.fepmPoint = leaderBoard.fepmPoint
                        isExceededMaxPoint = true
                        data = hashMapOf("fepmPoint" to points)}
                }
            }
            ExamTypeEnum.IP.toString() ->{ if(leaderBoard.ippoint < points) leaderBoardPoint.ippoint = points else leaderBoardPoint.ippoint = leaderBoard.ippoint
                isExceededMaxPoint = true
                data = hashMapOf("ippoint" to points)}
        }
        if(isExceededMaxPoint){
                db.collection("leaderBoard")
                    .document(SPManager(this).getUserUid())
                    .set(if(noLeaderBoard)leaderBoardPoint else data , SetOptions.merge())
                    .addOnSuccessListener {
                        Log.e("leader","add success =>$it")
                    }
                    .addOnFailureListener {
                        Log.e("leader","add error =>$it")

                    }
                    .addOnCompleteListener {
                        finish()
                    }
            }


    }
    private var leaderBoard = LeaderBoardPoint()
    private var noLeaderBoard = false
    private fun getLeaderBoard(){
        db.collection("leaderBoard")
            .whereEqualTo("uid",SPManager(this).getUserUid())
            .get()
            .addOnSuccessListener {
                if(it.documents.size > 0 ){
                    leaderBoard = Gson().fromJson(Gson().toJson(it.documents[0].data),LeaderBoardPoint::class.java)
                    Log.e("leader","get success =>$leaderBoard")
                }else{
                    noLeaderBoard = true
                }


            }
            .addOnFailureListener {
                Log.e("leader","get error =>$it")
            }
    }
//    private fun changeAnswersBackground(){
//        answer1.setOnClickListener {
//            changeViewColor(it)
//        }
//        answer2.setOnClickListener {
//            changeViewColor(it)
//        }
//        answer3.setOnClickListener {
//            changeViewColor(it)
//        }
//        answer4.setOnClickListener {
//            changeViewColor(it)
//        }
//    }
//    private fun changeViewColor(view: View){
//        when(view){
//            answer1 ->{
//                answer1.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke_blue)
//                answer2.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer3.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer4.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//            }
//            answer2 ->{
//                answer1.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer2.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke_blue)
//                answer3.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer4.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//            }
//            answer3 ->{
//                answer1.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer2.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer3.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke_blue)
//                answer4.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//            }
//            answer4 ->{
//                answer1.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer2.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer3.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke)
//                answer4.background = ContextCompat.getDrawable(this,R.drawable.rect_stroke_blue)
//            }
//        }
//    }

}
