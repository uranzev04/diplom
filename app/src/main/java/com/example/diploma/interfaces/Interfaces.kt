package com.example.diploma.interfaces
/**
 *Created By Uranzev
 * 2019
 */
interface RecyclerItemClickListener{
    fun onItemClickListener(position:Int, data:Any)


}
interface DialogButtonClickListener{
    fun onButtonClicked(buttonType:Int)
}