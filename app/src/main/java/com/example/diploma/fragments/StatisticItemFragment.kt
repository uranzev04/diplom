package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry

import com.example.diploma.R
import kotlinx.android.synthetic.main.fragment_statistic_item.*

/**
 * A simple [Fragment] subclass.
 */
class StatisticItemFragment(private val chartType:String, private val list: List<Float>) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistic_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = ArrayList<DataEntry>()
        list.mapIndexed { index, i ->
            when(index){
                0 -> {
                    if(i>0){
                        data.add(ValueDataEntry("FE өглөө", i))
                    }
                }
                1 -> {
                    if(i>0){
                        data.add(ValueDataEntry("FE өдөр", i))
                    }

                }
                2 -> {
                    if(i>0){
                        data.add(ValueDataEntry("IP", i))
                    }
                }
                3 -> {
                    if(i>0){
                        data.add(ValueDataEntry("AP өглөө", i))
                    }
                }
                4 -> {
                    if(i>0){
                        data.add(ValueDataEntry("AP өдөр", i))
                    }
                }
            }
        }
        when(chartType){
            "pie" ->{
                val pie = AnyChart.pie()
                pie.data(data)
                anyChartView.setChart(pie)
                statisticItemText.text = getString(R.string.countOfExams)

            }
            "column"->{
                val column = AnyChart.column3d()
                column.data(data)
                anyChartView.setChart(column)
                statisticItemText.text = getString(R.string.averageOfExams)
            }
            else ->{}
        }
    }


}
