package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.example.diploma.ExamTypeEnum
import com.example.diploma.History

import com.example.diploma.R
import com.example.diploma.SPManager
import com.example.diploma.adapters.StatisticItemAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_statistic.*

/**
 * Created by Uranzev 2019
 */
class StatisticFragment : Fragment() {
    private lateinit var db: FirebaseFirestore
    private lateinit var adapter:StatisticItemAdapter
    private val  histories = ArrayList<History>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = FirebaseFirestore.getInstance()

        statisticBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }
        getHistories()
    }

    private fun getHistories(){
        db.collection("history")
            .whereEqualTo("uid", SPManager(activity!!).getUserUid())
            .get()
            .addOnSuccessListener {
                for (document in it){
                    val history = Gson().fromJson(Gson().toJson(document.data),History::class.java)
                    Log.e("history","isAm ->${history.am}")
                    histories.add(history)
                }
                calculateChart()
            }
            .addOnFailureListener {
                Log.e("history","error =>$it")
            }
    }

    private fun calculateChart(){
        var feMorningCount = 0F
        var feDayCount = 0F
        var ipCount = 0F
        var apMorningCount = 0F
        var apDayCount = 0F
        var avgFeMorning = 0F
        var avgFeDay = 0F
        var avgIp = 0F
        var avgApMorning = 0F
        var avgAPday = 0F


        histories.mapIndexed { index, history ->
            when (history.examType) {
                ExamTypeEnum.FE.toString() -> {
                    if(history.am == 1){
                        feMorningCount = feMorningCount.inc()
                        avgFeMorning += getPercent(history.correctAnswer, history.totalAnswer)
                        Log.e("statistic","ave==>${getPercent(history.correctAnswer, history.totalAnswer)}")
                    } else  {
                        feDayCount = feDayCount.inc()
                        avgFeDay += getPercent(history.correctAnswer, history.totalAnswer)
                        Log.e("statistic","ave==>${getPercent(history.correctAnswer, history.totalAnswer)}")

                    }

                }
                ExamTypeEnum.IP.toString() -> {
                    ipCount = ipCount.inc()
                    avgIp += getPercent(history.correctAnswer, history.totalAnswer)
                    Log.e("statistic","ave==>${getPercent(history.correctAnswer, history.totalAnswer)}")

                }
                ExamTypeEnum.AP.toString() -> {
                    if(history.am == 1){
                        apMorningCount = apMorningCount.inc()
                        avgApMorning += getPercent(history.correctAnswer, history.totalAnswer)
                        Log.e("statistic","ave==>${getPercent(history.correctAnswer, history.totalAnswer)}")

                    } else {
                        apDayCount = apDayCount.inc()
                        avgAPday +=  getPercent(history.correctAnswer, history.totalAnswer)
                        Log.e("statistic","ave==>${getPercent(history.correctAnswer, history.totalAnswer)}")

                    }
                }

                else ->{}
            }
        }
        val counts = listOf(feMorningCount,feDayCount,ipCount,apMorningCount,apDayCount)
        val averages = listOf(avgFeMorning/feMorningCount,avgFeDay/feDayCount,avgIp/ipCount,avgApMorning/apMorningCount, avgAPday/apDayCount)
        initChart(counts,averages)
    }

    private fun getPercent(received:Int, total:Int ):Float{
        Log.e("statistic","receiver=>$received total==>$total")
       return if(received>0)(received.toFloat()/total)*100 else 0F
    }

    private fun initChart(counts:List<Float>, averages:List<Float>){
        adapter = StatisticItemAdapter(activity!!.supportFragmentManager, counts,averages)
        statisticViewPager.adapter = adapter



    }


}
