package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diploma.*

import com.example.diploma.adapters.CheckAnswersAdapter
import com.example.diploma.interfaces.RecyclerItemClickListener
import kotlinx.android.synthetic.main.fragment_check_answers.*

/**
 * Created by Uranzev
 * 2019
 */
class CheckAnswersFragment : Fragment() {
    lateinit var questions:ArrayList<Questions>
    var correctAnswersCount = 0
    var wrongAnswersCount = 0
    var skippedAnswersCount = 0
    var receivedPoints = 0

    private lateinit var adapter:CheckAnswersAdapter
    private lateinit var layoutManager: LinearLayoutManager
    var answerResults = ArrayList<AnswerResultItem>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_check_answers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkAnswerButton.setOnClickListener {
            if(SPManager(activity!!).getExerciseYear() != 0){
                Log.e("exercise","finish")
                (activity as ExamActivity).finishActivity()
            }
            else
                (activity as ExamActivity).uploadHistory()
        }
        Toast.makeText(activity!!,getString(R.string.exam_finished),Toast.LENGTH_SHORT).show()
        setDatas()
    }

    private fun setDatas(){
        when(correctAnswersCount*100/questions.size){
            in 80..100 ->{
                star1.setImageResource(R.drawable.ic_star_black_24dp)
                star2.setImageResource(R.drawable.ic_star_black_24dp)
                star3.setImageResource(R.drawable.ic_star_black_24dp)
            }
            in 60..79 ->{
                star1.setImageResource(R.drawable.ic_star_black_24dp)
                star2.setImageResource(R.drawable.ic_star_black_24dp)
                star3.setImageResource(R.drawable.ic_star_white_24dp)
            }
            in 1..59 ->{
                star1.setImageResource(R.drawable.ic_star_black_24dp)
                star2.setImageResource(R.drawable.ic_star_white_24dp)
                star3.setImageResource(R.drawable.ic_star_white_24dp)
            }
            else ->{
                star1.setImageResource(R.drawable.ic_star_white_24dp)
                star2.setImageResource(R.drawable.ic_star_white_24dp)
                star3.setImageResource(R.drawable.ic_star_white_24dp)
            }
        }
        correctAnswersText.text = correctAnswersCount.toString() +"/"+questions.size
        skippedAnswersText.text = skippedAnswersCount.toString() +"/"+questions.size
        wrongAnswersText.text = wrongAnswersCount.toString() +"/"+questions.size
        checkAnswerPoint.text = getString(R.string.point)+receivedPoints
        adapter = CheckAnswersAdapter(object:RecyclerItemClickListener{
            override fun onItemClickListener(position: Int, data: Any) {

            }

        })
        Log.e("check","data =>$answerResults")
        adapter.setData(answerResults)
        layoutManager = LinearLayoutManager(activity!!)
        checkAnswersRecyclerView.layoutManager = layoutManager
        checkAnswersRecyclerView.adapter = adapter
    }


}
