package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.example.diploma.LeaderBoardPoint

import com.example.diploma.R
import com.example.diploma.adapters.LeaderBoardPagerAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_leader_board.*

/**
 * Created by Uranzev 2019
 */
class LeaderBoardFragment : Fragment() {
    lateinit var adapter:LeaderBoardPagerAdapter
    lateinit var layoutManager: LinearLayoutManager
    lateinit var db:FirebaseFirestore
    private var leaderBoards = ArrayList<LeaderBoardPoint>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_leader_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = FirebaseFirestore.getInstance()
        getLeaderBoards()
        changePages(0)
        changeViewPagerPages()
        leaderBoardBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }


    }

    private fun getLeaderBoards(){
        leaderBoards.clear()
        db.collection("leaderBoard")
            .get()
            .addOnSuccessListener {
                for(document in it){
                    val board = Gson().fromJson(Gson().toJson(document.data),LeaderBoardPoint::class.java)
                    leaderBoards.add(board)
                    Log.e("leaderBoard","data = >${board.name}")
                }
                initViewPager()
            }
            .addOnFailureListener {
                Log.e("leaderBoard","error= >${it}")
            }
    }

    private fun changeViewPagerPages(){
        viewPagerTab1.setOnClickListener {
            changePages(0)
        }
        viewPagerTab2.setOnClickListener {
            changePages(1)
        }
        viewPagerTab3.setOnClickListener {
            changePages(2)
        }
        viewPagerTab4.setOnClickListener {
            changePages(3)
        }
        viewPagerTab5.setOnClickListener {
            changePages(4)
        }
    }

    private fun changePages(position:Int){
        leaderBoardViewPager.currentItem = position
        when(position){
            0 ->{
                viewPagerTab1.background = ContextCompat.getDrawable(activity!!,R.drawable.fill_rounded_left)
                viewPagerTab2.background = null
                viewPagerTab3.background = null
                viewPagerTab4.background = null
                viewPagerTab5.background = null
                changeColor(viewPagerTab1Text1, viewPagerTab1Text2, R.color.colorWhite)
                changeColor(viewPagerTab2Text1, viewPagerTab2Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab3Text1, viewPagerTab3Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab4Text1, viewPagerTab4Text2, R.color.colorPaleBlue)
                viewPagerTab5Text.setTextColor(activity!!.resources.getColor(R.color.colorPaleBlue))

            }
            1 ->{
                viewPagerTab1.background = null
                viewPagerTab2.background = ContextCompat.getDrawable(activity!!,R.drawable.rectange_blue)
                viewPagerTab3.background = null
                viewPagerTab4.background = null
                viewPagerTab5.background = null
                changeColor(viewPagerTab1Text1, viewPagerTab1Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab2Text1, viewPagerTab2Text2, R.color.colorWhite)
                changeColor(viewPagerTab3Text1, viewPagerTab3Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab4Text1, viewPagerTab4Text2, R.color.colorPaleBlue)
                viewPagerTab5Text.setTextColor(activity!!.resources.getColor(R.color.colorPaleBlue))
            }
            2 ->{
                viewPagerTab1.background = null
                viewPagerTab2.background = null
                viewPagerTab3.background = ContextCompat.getDrawable(activity!!,R.drawable.rectange_blue)
                viewPagerTab4.background = null
                viewPagerTab5.background = null
                changeColor(viewPagerTab1Text1, viewPagerTab1Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab2Text1, viewPagerTab2Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab3Text1, viewPagerTab3Text2, R.color.colorWhite)
                changeColor(viewPagerTab4Text1, viewPagerTab4Text2, R.color.colorPaleBlue)
                viewPagerTab5Text.setTextColor(activity!!.resources.getColor(R.color.colorPaleBlue))
            }
            3 ->{
                viewPagerTab1.background = null
                viewPagerTab2.background = null
                viewPagerTab3.background = null
                viewPagerTab4.background = ContextCompat.getDrawable(activity!!,R.drawable.rectange_blue)
                viewPagerTab5.background = null
                changeColor(viewPagerTab1Text1, viewPagerTab1Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab2Text1, viewPagerTab2Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab3Text1, viewPagerTab3Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab4Text1, viewPagerTab4Text2, R.color.colorWhite)
                viewPagerTab5Text.setTextColor(activity!!.resources.getColor(R.color.colorPaleBlue))
            }
            4 ->{
                viewPagerTab1.background = null
                viewPagerTab2.background = null
                viewPagerTab3.background = null
                viewPagerTab4.background = null
                viewPagerTab5.background = ContextCompat.getDrawable(activity!!,R.drawable.fill_rounded_right)
                changeColor(viewPagerTab1Text1, viewPagerTab1Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab2Text1, viewPagerTab2Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab3Text1, viewPagerTab3Text2, R.color.colorPaleBlue)
                changeColor(viewPagerTab4Text1, viewPagerTab4Text2, R.color.colorPaleBlue)
                viewPagerTab5Text.setTextColor(activity!!.resources.getColor(R.color.colorWhite))
            }
        }
    }

    fun changeColor(text1:TextView, text2:TextView, color:Int){
        text1.setTextColor(activity!!.resources.getColor(color))
        text2.setTextColor(activity!!.resources.getColor(color))
    }

    private fun initViewPager(){
        if(leaderBoards.size > 0){
            noLeaderBoardText.visibility = View.GONE
            layoutManager = LinearLayoutManager(activity!!)
            adapter = LeaderBoardPagerAdapter(activity!!.supportFragmentManager,leaderBoards)
            leaderBoardViewPager.adapter = adapter
            leaderBoardViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {
                    changePages(position)
                }

            })
        }else{
            noLeaderBoardText.visibility = View.VISIBLE
        }

    }


}
