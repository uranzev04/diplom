package com.example.diploma.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.diploma.*

import kotlinx.android.synthetic.main.fragment_exercise.*

/**
 * A simple [Fragment] subclass.
 */
class ExerciseFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        exerciseFragmentBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        initSpinner()
        morningCheckBox.setOnClickListener {
            SPManager(activity!!).setExerciseIsam(morningCheckBox.isChecked)
        }
        exerciseButton.setOnClickListener {
            startExercise()
        }
    }

    private fun startExercise(){
        val intent = Intent(activity!!, ExamActivity::class.java)
        intent.putExtra("exam",false)
        startActivity(intent)
    }

    private fun initSpinner(){
        val yearList:List<Int> = resources.getStringArray(R.array.years).map { it.toInt() }
        exerciseYearSpinner.adapter = ArrayAdapter<Int>(activity!!,android.R.layout.simple_list_item_1, yearList )
        exerciseYearSpinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                SPManager(activity!!).setExerciseYear(yearList[position])
            }

        }

        val typeList = resources.getStringArray(R.array.types)
        exerciseTypeSpinner.adapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_list_item_1, typeList )
        exerciseTypeSpinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> SPManager(activity!!).setExerciseType(ExamTypeEnum.FE.toString())
                    1 -> SPManager(activity!!).setExerciseType(ExamTypeEnum.IP.toString())
                    2 -> SPManager(activity!!).setExerciseType(ExamTypeEnum.AP.toString())
                }

            }

        }
        val fieldList = resources.getStringArray(R.array.fields)
        exerciseFieldSpinner.adapter = ArrayAdapter<String>(activity!!,android.R.layout.simple_list_item_1, fieldList )
        exerciseFieldSpinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> SPManager(activity!!).setExerciseField(QuestionField.technology.toString())
                    1 -> SPManager(activity!!).setExerciseField(QuestionField.management.toString())
                    2 -> SPManager(activity!!).setExerciseField(QuestionField.strategy.toString())
                }

            }

        }
    }



}
