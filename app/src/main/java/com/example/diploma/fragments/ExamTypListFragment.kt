package com.example.diploma.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diploma.ExamType
import com.example.diploma.MainActivity

import com.example.diploma.R
import com.example.diploma.adapters.MainExamTypeAdapter
import com.example.diploma.interfaces.RecyclerItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_exam_typ_list.*

/**
 * A simple [Fragment] subclass.
 */
class ExamTypListFragment(var examTypes:ArrayList<ExamType>, val isEnglish:Boolean) : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exam_typ_list, container, false)
    }
    private lateinit var adapter : MainExamTypeAdapter
    private lateinit var layoutManager: LinearLayoutManager
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = MainExamTypeAdapter(object: RecyclerItemClickListener {
            override fun onItemClickListener(position: Int, data: Any) {
                (data as ExamType)
                (activity as MainActivity).startExam(data)
            }
        },isEnglish)
        adapter.setDatas(examTypes)
        layoutManager = LinearLayoutManager(activity!!)
        examTypeListRecyclerView.layoutManager = layoutManager
        examTypeListRecyclerView.adapter = adapter
    }




}
