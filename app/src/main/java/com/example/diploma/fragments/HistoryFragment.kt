package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diploma.History

import com.example.diploma.R
import com.example.diploma.SPManager
import com.example.diploma.adapters.HistoryAdapter
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_history.*

/**
 * Created by Uranzev 2019
 */
class HistoryFragment : Fragment() {
    private lateinit var adapter:HistoryAdapter
    private lateinit var layoutManager:LinearLayoutManager
    private var histories = ArrayList<History>()
    private lateinit var db:FirebaseFirestore
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = FirebaseFirestore.getInstance()
        getHistory()
        historyFragmentBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }
    }

    private fun getHistory(){
        db.collection("history")
            .whereEqualTo("uid",SPManager(activity!!).getUserUid())
            .get()
            .addOnSuccessListener {
                histories.clear()
                for (document in it){
                    val history = Gson().fromJson(Gson().toJson(document.data),History::class.java)
                    Log.e("history","isAm ->${history.am}")
                    histories.add(history)
                }
                initAdapter()
            }
            .addOnFailureListener {
                Log.e("history","error =>$it")
            }
    }

    private fun initAdapter(){
        if(histories.size>0){
            noHistoryText.visibility = View.GONE
            adapter = HistoryAdapter()
            histories.sortByDescending { it.date }
            adapter.setData(histories)
            layoutManager = LinearLayoutManager(activity!!)
            historyRecyclerView.layoutManager = layoutManager
            historyRecyclerView.adapter = adapter
        }else{
            noHistoryText.visibility = View.VISIBLE
        }


    }


}
