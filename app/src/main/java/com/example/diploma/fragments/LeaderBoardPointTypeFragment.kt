package com.example.diploma.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diploma.LeaderBoardPoint
import com.example.diploma.LeaderBoardTypePoint

import com.example.diploma.R
import com.example.diploma.adapters.LeaderBoardAdapter
import kotlinx.android.synthetic.main.fragment_leader_board_point_type.*

/**
 * Created by Uranzev 2019
 */
class LeaderBoardPointTypeFragment(var type:String, var leaderBoards:ArrayList<LeaderBoardPoint> ) : Fragment() {
    var leaderBoardType = ArrayList<LeaderBoardTypePoint>()
    lateinit var adapter:LeaderBoardAdapter
    lateinit var layoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_leader_board_point_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("leaderBoard","boars =>$leaderBoards")
        leaderBoardType.clear()
        for (i in 0 until leaderBoards.size){
            initDatas(i)
        }
        initAdapter()
    }

    private fun initDatas(i:Int){
        val boardType = LeaderBoardTypePoint()
        boardType.point = when(type){
            "apamPoint" ->{ leaderBoards[i].apamPoint }
            "appmPoint" ->{leaderBoards[i].appmPoint}
            "feamPoint" ->{leaderBoards[i].feamPoint}
            "fepmPoint" ->{leaderBoards[i].fepmPoint}
            "ippoint" ->{leaderBoards[i].ippoint}
            else ->0
        }
        boardType.name = leaderBoards[i].name
        boardType.url = leaderBoards[i].photoUrl
        leaderBoardType.add(boardType)
    }

    private fun initAdapter(){
        layoutManager = LinearLayoutManager(activity!!)
        adapter = LeaderBoardAdapter()
        adapter.setData(leaderBoardType)
        leaderBoardTypeRecyclerView.layoutManager = layoutManager
        leaderBoardTypeRecyclerView.adapter = adapter
    }
}
