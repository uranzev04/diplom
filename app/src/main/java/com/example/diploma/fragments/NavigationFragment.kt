package com.example.diploma.fragments


import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.diploma.*

import com.example.diploma.commonUtils.CommonDialog
import com.example.diploma.commonUtils.showCircleImage
import com.example.diploma.interfaces.DialogButtonClickListener
import com.example.diploma.login.LoginActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_navigation.*

/**
 *Created By Uranzev
 * 2019
 */
class NavigationFragment : Fragment() {
    private lateinit var db: FirebaseFirestore
    private var user = User()
    private var uid = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = FirebaseFirestore.getInstance()
        uid = SPManager(activity!!).getUserUid()
        readUsersFromFireBase()
        setDatas()
        logOut.setOnClickListener {
            val property = ConfirmDialogProperty()
            property.title = getString(R.string.wantLogOut)
            property.positiveText = getString(R.string.yes)
            property.negativeText = getString(R.string.no)
            CommonDialog.showConfirmDialog(activity!!,property, object:DialogButtonClickListener{
                override fun onButtonClicked(buttonType: Int) {
                    if(buttonType == DialogInterface.BUTTON_POSITIVE){
                        logOut()
                    }
                }
            })

        }

        historyContainer.setOnClickListener {
            (activity as MainActivity).showHistoryFragment()
        }
        leaderBoardContainer.setOnClickListener {
            (activity as MainActivity).showLeaderBoardFragment()
        }

        statisticContainer.setOnClickListener {
            (activity as MainActivity).showStatisticFragment()
        }

        exerciseContainer.setOnClickListener {
            (activity as MainActivity).showExerciseFragment()
        }

    }
    private fun logOut(){
        FirebaseAuth.getInstance().signOut()
        SPManager(activity!!).resetAll()
        callLoginActivity()
    }
    private fun callLoginActivity(){
        val intent = Intent(activity!!,LoginActivity::class.java)
        startActivity(intent)
    }
    private fun readUsersFromFireBase(){
        db.collection("users")
            .get()
            .addOnSuccessListener {
                for(document in it){
                    Log.e("document", "${document.id} ==> ${document.data}")
                     user = document.toObject(User::class.java)
                }
            }
            .addOnFailureListener {
                Log.e("document","error")
            }
    }

    private fun setDatas(){
        navigationName.text = if(SPManager(activity!!).getUserName().isNotEmpty()) SPManager(activity!!).getUserName() else getString(R.string.user)
        showCircleImage(SPManager(activity!!).getUserAvatar(), navigationAvatar,context = activity!!)
    }

}
